const express = require('express')
const app = express()
const port = 3000
const client = require('twilio')('AC39cb59cb8235f83509c3c7af07e08808', '08dbb20d15a5c33292c7a399f96bb341');

app.use(express.json())
app.use(express.urlencoded({extended:false}))

app.get('/api/twilioTest', (req, res) => {
    client
        .calls
        .create({
            to: '+918460847060',
            from: '+12513177048',
            twiml: '<Response><Say voice="man" loop="1">Welcome to IBL Infotech, if you want to know anything about IBL infotech then contact us</Say></Response>',
            language: 'en-IN'
        })
        .then(call => {
            console.log(call.sid)
            res.send(call)
        })
        .catch(err => {
            console.log(err)
            res.send(err)
        });
});
// console.log(__filename);

app.listen(port, () => {
    console.log(`server running on port ${port}`)
})